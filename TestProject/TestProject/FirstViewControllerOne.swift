//
//  FirstViewControllerOne.swift
//  TestProject
//
//  Created by brubru on 19.05.2023.
//

import Foundation
import UIKit

// MARK: - FirstViewControllerOne

class FirstViewControllerOne: UIViewController {
	let button: UIButton = {
		let button = UIButton()
		button.setTitle("Done", for: .normal)
		button.tintColor = .white
		button.backgroundColor = .blue
		return button
	}()
	
	
		/// Label
	let label: UILabel = {
		let label = UILabel()
		label.text = "Hi"
		label.textColor = .black
		return label
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .red
		view.addSubview(button)
		view.addSubview(label)
		layer()
	}
	private func layer() {
		button.translatesAutoresizingMaskIntoConstraints = false
		label.translatesAutoresizingMaskIntoConstraints = false
		
		NSLayoutConstraint.activate([
			label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
			
			button.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
			button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			button.widthAnchor.constraint(equalToConstant: 100)
		])
	}
}


//
//  Person.swift
//  TestProject
//
//  Created by brubru on 12.06.2023.
//

import Foundation


	/// Модель данных пользователя
struct Person {
		/// Имя пользователя.
	let name = "Tom"
		/// Фамилия пользователя.
	let surename = "Ivanov"
		/// Возраст пользователя.
	let age = 90
}

//
//  User.swift
//  TestProject
//
//  Created by brubru on 12.06.2023.
//

import Foundation

	/// Модель аккаунта пользователя.
struct User {
		/// Логин пользователя.
	let login = "Tim"
		/// Пароль пользователя.
	let password: String
}

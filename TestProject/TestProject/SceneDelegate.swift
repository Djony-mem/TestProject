//
//  SceneDelegate.swift
//  TestProject
//
//  Created by brubru on 15.05.2023.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	var window: UIWindow?

	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        let firstVC = FirstViewControllerOne()
        window?.rootViewController = firstVC 
        window?.makeKeyAndVisible()
	}
}

